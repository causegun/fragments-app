package com.example.fragmentsapp

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

class FirstFragment : Fragment() {
    private lateinit var fragmentListener: FragmentListener
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is FragmentListener)
            fragmentListener = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val firstFragmentView = inflater.inflate(R.layout.first_fragment, container, false)
        val changeColorsButton = firstFragmentView.findViewById<Button>(R.id.change_colors)
        val switchFragmentsButton = firstFragmentView.findViewById<Button>(R.id.swap_fragments)

        changeColorsButton.setOnClickListener {
            fragmentListener.onChangeColorClick()
        }
        switchFragmentsButton.setOnClickListener {
            fragmentListener.onSwapFragmentsClick()
        }

        return firstFragmentView
    }

    interface FragmentListener {
        fun onChangeColorClick()
        fun onSwapFragmentsClick()
    }
}