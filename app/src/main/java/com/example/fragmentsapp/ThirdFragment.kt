package com.example.fragmentsapp

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment

class ThirdFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.third_fragment, container, false)
        val textView = view.findViewById<TextView>(R.id.third_fragment_text)
        textView.text = savedInstanceState?.getString("TITLE") ?: "Third Fragment"
        view.setBackgroundColor(savedInstanceState?.getInt("BACKGROUND_COLOR") ?: Color.WHITE)
        return view
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("BACKGROUND_COLOR", (view?.background as ColorDrawable).color)
        outState.putString(
            "TITLE",
            view?.findViewById<TextView>(R.id.third_fragment_text)?.text.toString()
        )
    }
}