package com.example.fragmentsapp

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity(), FirstFragment.FragmentListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (supportFragmentManager.findFragmentById(R.id.fragment_container3) == null) {
            val fragment1 = FirstFragment()
            val fragment2 = SecondFragment()
            val fragment3 = ThirdFragment()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_container1, fragment1)
                .add(R.id.fragment_container2, fragment2)
                .add(R.id.fragment_container3, fragment3)
                .commit()
        }
    }

    override fun onChangeColorClick() {
        val fragment2 = supportFragmentManager
            .findFragmentById(R.id.fragment_container2)
        val fragment3 = supportFragmentManager
            .findFragmentById(R.id.fragment_container3)
        val color1 = Color.argb(255, (0..256).random(), (0..256).random(), (0..256).random())
        val color2 = Color.argb(255, (0..256).random(), (0..256).random(), (0..256).random())
        fragment2?.view?.setBackgroundColor(color1)
        fragment3?.view?.setBackgroundColor(color2)
    }

    override fun onSwapFragmentsClick() {
        val fragment2 = supportFragmentManager
            .findFragmentById(R.id.fragment_container2)
        val fragment3 = supportFragmentManager
            .findFragmentById(R.id.fragment_container3)

        val text2 = fragment2?.view?.findViewById<TextView>(R.id.second_fragment_text)?.text
        val color2 = (fragment2?.view?.background as ColorDrawable).color

        fragment2.view?.findViewById<TextView>(R.id.second_fragment_text)?.text =
            fragment3?.view?.findViewById<TextView>(R.id.third_fragment_text)?.text

        (fragment2.view?.background as ColorDrawable).color =
            (fragment3?.view?.background as ColorDrawable).color

        fragment3.view?.findViewById<TextView>(R.id.third_fragment_text)?.text = text2

        (fragment3.view?.background as ColorDrawable).color = color2
    }
}