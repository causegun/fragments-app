# Fragments App

Create an activity that contains 3 fragments. You need to take the following steps:

Fragment1 contains two buttons:
- A button that changes the background color of Fragment2 and Fragment3. You can use random colors or use the background color of Fragment2 for Fragment3 and vice versa. It's up to you.
- A button that swaps Fragment2 to Fragment3 and vice versa.
- Fragment2 and Fragment3 have an identifying fragment element (ex: a label with the text "Fragment 2" and "Fragment 3").
- Add the option of saving the state of fragments after rotation (fragments' positions and background colors).
